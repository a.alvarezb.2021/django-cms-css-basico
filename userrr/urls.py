from django.urls import path

from . import views

urlpatterns = [
    path('css', views.style, name='style'),
    path('', views.index),
    path("logout", views.logout_view),
    path("<str:llave>", views.do_MEHTOD),
    ]