from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
import random
from .models import Contenido

# Create your views here.

def index(request):
    """
    SI me hace un GET /cms/ le doy una respueta de que esta es un cms app

    """
    contenidos = Contenido.objects.all()
    if request.user.is_authenticated:
        logged = "Logged in as " + request.user.username + " <a href='/cms/logout'>Logout</a>"
        return render(request, "userrr/index.html", {"logged":logged,"contenidos": contenidos, "request":request})
    else:
        logged = "Not logged in. <a href='/admin/'>Login via admin</a>"
        return render(request, "userrr/index.html", {"logged":logged,"contenidos": contenidos, "request":request})



@csrf_exempt
def do_MEHTOD(request, llave):
    """
    En caso de que el metodo sea GET , obtengo el el body del objeto guardado con la llave = llave
    y devuelvo su contenido. En caso de no exista contenido para esa llave salta la excepcion
    En caso de que el metodo sea PUT, obtengo el cuepor de la peticion y me creop un objeto del tipo contenido,
    guardango su llave y su valor. Luego devuelvo el body del objeto creado.
    """

    html = ""
    if request.method == "GET":
        try:
            contenido = Contenido.objects.get(llave=llave)
            return render(request, "userrr/object.html", {"llave": contenido.llave, "valor": contenido.valor})
        except Contenido.DoesNotExist:  # Aquí está la corrección
            logout(request)
            return redirect("/cms/")

    elif request.method == "PUT":
        cuerpo = request.body.decode("utf-8")
        if cuerpo:
            contenido = Contenido(llave=llave, valor=cuerpo)
            contenido.save()
        try:
            respuesta = Contenido.objects.get(llave=llave).valor
            html = f"Guardando en un nuevo contenido la llave {llave} con el cuerpo : {respuesta}"
        except Contenido.DoesNotExist:
            html += f"No hay contenido para la llave: {llave}"
    elif request.method == "POST":
        new_content = request.POST["cuerpo"]
        try:
            c = Contenido.objects.get(llave=llave)
            c.valor = new_content
            c.save()
            return render(request, "userrr/object.html", {"llave": llave, "valor": new_content})
        except Contenido.DoesNotExist:
            html += f"No hay contenido para la llave: {llave}"
    return HttpResponse(html)

@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect("/cms/")


colors = ['blue', 'gold', 'red', 'aqua', 'green', 'black', 'yellow']

def style(request):
    color = random.choice(colors)
    background = random.choice(colors)
    return render(request, "userrr/main.css", {"color":color, "background": background}, content_type="text/css")